<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  This stylesheet generates XML chunks, but in nested folders, according
  to the structure of the document.
-->


<xsl:import href="chunk-plain.xsl"/>

<!-- include a customized version of the template "chunk-filename" -->
<xsl:include href="../template/chunk-filename.xsl"/>


</xsl:stylesheet>
