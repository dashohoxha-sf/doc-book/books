<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  This is a customization of the docbook formating stylesheets, which
  does not format the XML content to convert it to XHTML, but instead
  copies it verbatim, with all the elements and attributes that are there.
  This is achieved by overriding the template of each element to copy
  recursivly each attribute and subelement.
-->

<xsl:import
  href="http://docbook.sourceforge.net/release/xsl/current/xhtml/docbook.xsl"/>

<!-- copy everything recursivly -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
