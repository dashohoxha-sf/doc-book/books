#!/bin/bash

book_path=../../../content/books/xml/docbookwiki_guide/en
xml_chunk=$book_path/docbookwiki_guide/administrating.xml
new_id=admin

xsltproc --stringparam new_id $new_id \
         ../change_chunk_id.xsl $xml_chunk

