#!/bin/bash

book_path=../../../content/books/xml/docbookwiki_guide/en
xml_chunk=$book_path/docbookwiki_guide/administrating.xml
href=administrating/new_chunk.xml

xsltproc --stringparam href $href ../add_subchunk.xsl $xml_chunk

