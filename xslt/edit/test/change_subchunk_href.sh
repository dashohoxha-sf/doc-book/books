#!/bin/bash

book_path=../../../content/books/xml/docbookwiki_guide/en
xml_chunk=$book_path/docbookwiki_guide/administrating.xml
old_href=administrating/book_editors.xml
new_href=administrating/book_editors_1.xml

xsltproc --stringparam old_href $old_href \
         --stringparam new_href $new_href \
         ../change_subchunk_href.xsl $xml_chunk

