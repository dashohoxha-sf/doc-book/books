#!/bin/bash

book_path=../../../content/books/xml/docbookwiki_guide/en
xml_chunk=$book_path/docbookwiki_guide/administrating.xml
href=administrating/book_editors.xml

xsltproc --stringparam href $href ../move_up.xsl $xml_chunk
