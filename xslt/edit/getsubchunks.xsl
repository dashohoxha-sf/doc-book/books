<?xml version="1.0"?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
  Outputs a list of the subchunks of a certain chunk (their ids and 
  titles), like this:

  subchunkid1 Subnode1 Title
  subchunkid2 Title of the Second Subnode

  Is called with parameters book_dir and path, like this:
  xsltproc getsubchunks.xsl $chunk_path/chunk.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" encoding="utf-8" />

<xsl:variable name="separator" select="' '" />
<xsl:variable name="eol">
  <xsl:text>
</xsl:text>
</xsl:variable>

<!-- match <xi:include> in the chunk -->
<xsl:template match="xi:include" xmlns:xi="http://www.w3.org/2001/XInclude">
  <xsl:value-of select="substring-before(substring-after(@href,'/'), '.xml')"/>
  <xsl:value-of select="$separator"/>
  <xsl:apply-templates mode="get-title" select="document(@href)" />
  <xsl:value-of select="$eol"/>
</xsl:template>

<!-- ignore the text nodes in the chunk -->
<xsl:template match="text()" />

<!-- match and output the title in the subchunk -->
<xsl:template mode="get-title" match="/*/title">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>
<!-- ignore the text nodes in the subchunks -->
<xsl:template mode="get-title" match="text()" />

</xsl:transform>
