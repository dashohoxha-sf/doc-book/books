<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
  Returns the type of a chunk and its first subchunk, 
  for example: 'chapter:section'. If the chunk has no subchunks,
  then the second part of the output is empty, for example: 'section:'.
  It is called , like this:  xsltproc get_chunk_type.xsl chunk.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" encoding="utf-8" />

<xsl:template match="/*">
  <xsl:value-of select="name(.)" />
  <xsl:text>:</xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="/*/xi:include[1]"
              xmlns:xi="http://www.w3.org/2001/XInclude">
  <xsl:apply-templates mode="get-subchunk-type" select="document(@href)" />
</xsl:template>

<xsl:template mode="get-subchunk-type" match="/*">
  <xsl:value-of select="name(.)" />
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />
<xsl:template match="text()" mode="get-subchunk-type" />

</xsl:transform>
