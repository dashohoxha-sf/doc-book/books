<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  Customized so that it just outputs the content
  and does not produce any headers, footers, etc.
-->

<xsl:template name="chunk-element-content">
  <xsl:param name="prev"/>
  <xsl:param name="next"/>
  <xsl:param name="nav.context"/>
  <xsl:param name="content">
    <xsl:apply-imports/>
  </xsl:param>

  <!-- output the content of the chunk -->
  <xsl:copy-of select="$content"/>

</xsl:template>


</xsl:stylesheet>
