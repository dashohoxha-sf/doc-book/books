<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  The template href.target is modified so that it makes a call to a
  javascript function.
-->

<xsl:template name="href.target">
  <xsl:param name="context" select="."/>
  <xsl:param name="object" select="."/>
  <xsl:param name="toc-context" select="."/>

  <xsl:variable name="chunkpath">
    <xsl:call-template name="chunk-path">
      <xsl:with-param name="object" select="$object"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="href">
    <xsl:text>javascript:goto_chunk('</xsl:text>
    <xsl:value-of select="$chunkpath"/>
    <xsl:text>')</xsl:text>
  </xsl:variable>

  <xsl:value-of select="$href"/>
</xsl:template>


<xsl:template name="chunk-path">
  <xsl:param name="object" select="."/>

  <xsl:variable name="href.to.uri">
    <xsl:call-template name="href.target.uri">
      <xsl:with-param name="object" select="$object"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:value-of select="substring-before($href.to.uri, $html.ext)"/>
</xsl:template>


</xsl:stylesheet>
