<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  This customization modifies the "process-chunk" template so that the chunks
  have <xi:include> elements that include their subchunks. This way
  the documents can be processed as a whole without having to be imploded.
-->


<xsl:template name="process-chunk">
  <xsl:param name="prev" select="."/>
  <xsl:param name="next" select="."/>
  <xsl:param name="content">
    <xsl:apply-imports/>
  </xsl:param>

  <xsl:variable name="ischunk">
    <xsl:call-template name="chunk"/>
  </xsl:variable>

  <xsl:variable name="chunkfn">
    <xsl:if test="$ischunk='1'">
      <xsl:apply-templates mode="chunk-filename" select="."/>
    </xsl:if>
  </xsl:variable>

  <xsl:if test="$ischunk='0'">
    <xsl:message>
      <xsl:text>Error </xsl:text>
      <xsl:value-of select="name(.)"/>
      <xsl:text> is not a chunk!</xsl:text>
    </xsl:message>
  </xsl:if>

  <xsl:variable name="filename">
    <xsl:call-template name="make-relative-filename">
      <xsl:with-param name="base.dir" select="$base.dir"/>
      <xsl:with-param name="base.name" select="$chunkfn"/>
    </xsl:call-template>
  </xsl:variable>


  <!-- added by dasho -->
  <!-- include the new chunk into the current document -->
  <xsl:variable name="file">
    <xsl:call-template name="get-relative-filename">
      <xsl:with-param name="filename" select="$chunkfn"/>
    </xsl:call-template>
  </xsl:variable>
  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="{$file}" />


  <xsl:call-template name="write.chunk">
    <xsl:with-param name="filename" select="$filename"/>
    <xsl:with-param name="content">
      <xsl:call-template name="chunk-element-content">
        <xsl:with-param name="prev" select="$prev"/>
        <xsl:with-param name="next" select="$next"/>
        <xsl:with-param name="content" select="$content"/>
      </xsl:call-template>
    </xsl:with-param>
    <xsl:with-param name="quiet" select="$chunk.quietly"/>
  </xsl:call-template>
</xsl:template>


<!-- added by dasho  -->
<!-- truncate the filename of the new chunk      -->
<!-- so that it is relative to the current chunk -->
<xsl:template name="get-relative-filename">
  <xsl:param name="filename" />

  <xsl:variable name="filename-1" select="substring-after($filename, '/')"/>
  <xsl:choose>
    <xsl:when test="contains($filename-1, '/')">
      <xsl:call-template name="get-relative-filename">
        <xsl:with-param name="filename" select="$filename-1" />
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$filename"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
