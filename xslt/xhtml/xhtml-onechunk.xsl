<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  This stylesheet generates XHTML chunks, but in nested folders, according
  to the structure of the document.
-->

<xsl:import href="xhtml-plain.xsl"/>

<!-- don't create a navigation file -->
<xsl:param name="create.navigation.file" select="'no'"/>

<!-- the parameter chunk should be provided by the calling script -->
<xsl:param name="chunk" select="'.'"/>

 
<xsl:template name="make.toc">
  <xsl:param name="toc-context" select="."/>
  <xsl:param name="toc.title.p" select="true()"/>
  <xsl:param name="nodes" select="/NOT-AN-ELEMENT"/>

  <xsl:variable name="toc.title">
    <xsl:if test="$toc.title.p">
      <p>
        <b>
          <xsl:call-template name="gentext">
            <xsl:with-param name="key">TableofContents</xsl:with-param>
          </xsl:call-template>
        </b>
      </p>
    </xsl:if>
  </xsl:variable>

  <xsl:element name="div">
    <xsl:attribute name="class"><xsl:value-of select="'toc'"/></xsl:attribute>
    <xsl:copy-of select="$toc.title"/>
    <xsl:element name="dl">
      <xsl:apply-templates mode="simple-toc" />
    </xsl:element>
  </xsl:element>

</xsl:template>

<xsl:template match="xi:include" xmlns:xi="http://www.w3.org/2001/XInclude"/>

<!-- match <xi:include> in the chunk -->
<xsl:template mode="simple-toc" match="xi:include"
              xmlns:xi="http://www.w3.org/2001/XInclude">

  <xsl:variable name="subchunk-id">
    <xsl:value-of select="substring-before(substring-after(@href,'/'), '.xml')"/>
  </xsl:variable>

  <xsl:variable name="subchunk-path">
    <xsl:value-of select="concat($chunk, '/', $subchunk-id)"/>
  </xsl:variable>

  <xsl:variable name="href">
    <xsl:text>javascript:goto_chunk('</xsl:text>
    <xsl:value-of select="$subchunk-path"/>
    <xsl:text>')</xsl:text>
  </xsl:variable>

  <xsl:variable name="subchunk-title">
    <xsl:apply-templates mode="get-title" select="document(@href)" />
  </xsl:variable>

  <xsl:element name="dt">
    <span class="section">
      <a href="{$href}"><xsl:value-of select="$subchunk-title"/></a>
    </span>
  </xsl:element>

</xsl:template>


<!-- match and output the title in the subchunk -->
<xsl:template mode="get-title" match="/*/title">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>


<!-- ignore the text nodes in the subchunks -->
<xsl:template mode="get-title" match="text()" />
<!-- ignore the text nodes in the subchunks -->
<xsl:template mode="simple-toc" match="text()" />


</xsl:stylesheet>
