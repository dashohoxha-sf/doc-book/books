<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
  This stylesheet is a customization of the docbook chunking stylesheets.
-->

<xsl:import href="docbook-easy.xsl"/>
<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk-common.xsl"/>
<xsl:include href="http://docbook.sourceforge.net/release/xsl/current/xhtml/manifest.xsl"/>
<xsl:include href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk-code.xsl"/>


<xsl:param name="chunker.output.omit-xml-declaration" select="'yes'"/>
<xsl:param name="chunker.output.doctype-public" select="''"/>
<xsl:param name="chunker.output.doctype-system" select="''"/>

<xsl:param name="chunker.output.encoding" select="'UTF-8'"/>
<xsl:param name="chunker.output.standalone" select="'yes'"/>
<xsl:param name="chunker.output.indent" select="'yes'"/>
<!-- <xsl:param name="chunker.output.cdata-section-elements" select="''"/> -->

<xsl:param name="root.filename" select="'main'"/>
<xsl:param name="use.id.as.filename" select="1"/>
<xsl:param name="html.ext" select="'.xhtml'"/>
<xsl:param name="chunk.first.sections" select="1"></xsl:param>
<xsl:param name="chunk.fast" select="1"/>
<xsl:param name="chunk.quietly" select="1"/>


<!-- customized so that it just outputs the content -->
<!-- and does not produce any headers, footers, etc.  -->
<xsl:include href="../template/chunk-element-content.xsl"/>

<!-- modified to link to 'javascript:goto_chunk(chunk_path)' -->
<xsl:include href="../template/href.target.xsl"/>

<!-- modified to create also a navigation file -->
<xsl:include href="../template/process-chunk.navigation.xsl"/>

<!-- suppress the title of the chunk -->
<xsl:template match="title" mode="titlepage.mode">
  <xsl:variable name="ischunk">
    <xsl:call-template name="chunk">
      <xsl:with-param name="node" select=".."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:if test="$ischunk='0'"> <xsl:apply-imports /> </xsl:if>
</xsl:template>

</xsl:stylesheet>
