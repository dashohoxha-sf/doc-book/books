//-*-C-*-
/*
  This file is part of  DocBookWiki.  DocBookWiki is a web application
  that displays and edits DocBook documents.

  Copyright (C) 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  DocBookWiki is free software;  you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  DocBookWiki is distributed  in the hope that it  will be useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along  with  DocBookWiki;  if   not,  write  to  the  Free  Software
  Foundation, Inc., 59 Temple  Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function set_lock(lock)
{
  SendEvent('edit', 'set_lock', 'lock='+lock);
}

function move_up(subchunk)
{
  SendEvent('edit', 'move_up', 'subchunk='+subchunk);
}

function move_down(subchunk)
{
  SendEvent('edit', 'move_down', 'subchunk='+subchunk);
}

function delete_chunk()
{
  var msg = T_("You are deleting this chunk and all the subchunks.");
  if (confirm(msg))
    {
      SendEvent('edit', 'delete');
    }
}

function change_id()
{
  var form = document.update;
  var id = form.id.value;

  if (id=='')
    {
      alert(T_("ID cannot be empty."));
      form.id.focus();
      return;
    }

  SendEvent('edit', 'change_id', 'id='+id);
}

function change_title()
{
  var form = document.update;
  var title = form.title.value;

  if (title=='')
    {
      alert(T_("Title cannot be empty."));
      form.title.focus();
      return;
    }

  SendEvent('edit', 'change_title', 'title='+title);
}

function add_subchunk()
{
  var form = document.add;
  var id = form.id.value;
  var title = form.title.value;
  var idx = form.chunk_type.selectedIndex;
  var chunk_type = form.chunk_type.options[idx].value;

  if (id=='')
    {
      alert(T_("Please give an ID."));
      form.id.focus();
      return;
    }
  if (title=='')
    {
      alert(T_("Please give a title."));
      form.title.focus();
      return;
    }

  var event_args = 'id='+id+';title='+title+';type='+chunk_type;
  //alert(event_args);
  SendEvent('edit', 'add_subchunk', event_args);
}
