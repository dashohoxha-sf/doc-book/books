<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package docbook
   * @subpackage edit
   */


include_once TPL.'common/funcs/validate.php';

  /**
   * This function sets a lock before the structure of the book
   * is modified, in order to make sure that the structure change
   * and update of the xhtml files happens as a single transaction,
   * and concurrent editing does not have unexpected results.
   * Since the structure of the book is the same for all the languages
   * and the changes are synchronized for all the languages, the lock
   * locks all the languages.
   */
function lock_book()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $lock_file = WS_BOOKS.$book_id.'/lock';

  //if the lock file exists, wait until 
  //it is removed or until it is too old
  while (file_exists($lock_file))
    {
      clearstatcache();
      $lock_age = time() - filectime($lock_file);
      if ($lock_age > 60) break;
    }
  shell("touch $lock_file");
}

function unlock_book()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $lock_file = WS_BOOKS.$book_id.'/lock';
  shell("rm $lock_file");
}

/** Append the chunk to the file 'modified_chunks.txt' */
function add_to_modified_chunks($chunk =UNDEFINED)
{
  if ($chunk==UNDEFINED)  $chunk = WebApp::getSVar('docbook->chunk');

  $book_id = WebApp::getSVar('docbook->book_id');
  $lng = WebApp::getSVar('docbook->lng');

  //get the lock for accessing the file
  $lock_file = WS_BOOKS."$book_id/$lng/modified_lock";
  while (file_exists($lock_file))
    {
      clearstatcache();
      $lock_age = time() - filectime($lock_file);
      if ($lock_age > 10) break;
    }
  shell("touch $lock_file");

  //modify the file
  $modified_chunks = WS_BOOKS."$book_id/$lng/modified_chunks.txt";
  shell("sed '\|^$chunk\$|d' -i $modified_chunks");
  shell("echo \"$chunk\" >> $modified_chunks");

  //release the lock
  shell("rm $lock_file");
}

/** Remove the chunk from the file 'modified_chunks.txt' */
function remove_from_modified_chunks($chunk =UNDEFINED, $recursive =false)
{
  if ($chunk==UNDEFINED)  $chunk = WebApp::getSVar('docbook->chunk');

  $book_id = WebApp::getSVar('docbook->book_id');
  $lng = WebApp::getSVar('docbook->lng');

  //get the lock for accessing the file
  $lock_file = WS_BOOKS."$book_id/$lng/modified_lock";
  while (file_exists($lock_file))
    {
      clearstatcache();
      $lock_age = time() - filectime($lock_file);
      if ($lock_age > 10) break;
    }
  shell("touch $lock_file");

  //modify the file
  $pattern = ($recursive ? "^$chunk" : "^$chunk\$");
  $modified_chunks = WS_BOOKS."$book_id/$lng/modified_chunks.txt";
  shell("sed '\|$pattern|d' -i $modified_chunks");

  //release the lock
  shell("rm $lock_file");
}

/*----------------------------------------------*/
    
/** add the recordset 'subchunks' */
function add_subchunks_rs()
{
  $subchunks = process_chunk('edit/getsubchunks.xsl');
  $lines = explode("\n", $subchunks);

  $chunk = WebApp::getSVar('docbook->chunk');

  $rs = new EditableRS('subchunks');
  for ($i=0; $i < sizeof($lines); $i++)
    {
      list($subchunk_id,$title) = split(' ', $lines[$i], 2);
      if ($subchunk_id=='')  continue;
      $title = trim($title);
      $subchunk = "$chunk/$subchunk_id";
      $rs->addRec(compact('subchunk', 'title'));
    }
  global $webPage;
  $webPage->addRecordset($rs);
}

/** 
 * Applies the given transformer to the given chunk and returns the result. 
 */
function process_chunk($transformer, $arr_params =array(), $chunk =UNDEFINED, 
                       $lng =UNDEFINED, $options ='')
{
  if ($chunk==UNDEFINED)     $chunk = WebApp::getSVar('docbook->chunk');
  if ($lng==UNDEFINED)       $lng   = WebApp::getSVar('docbook->lng');

  $book_id = WebApp::getSVar('docbook->book_id');

  //construct the list of stringparams
  $stringparams = '';
  while ( list($p_name, $p_value) = each($arr_params) )
    $stringparams .= "--stringparam $p_name \"$p_value\" ";

  $xml_file = WS_BOOKS."$book_id/$lng/$chunk.xml";
  $xsl_file = XSLT.$transformer;

  $cmd = "xsltproc $options $stringparams $xsl_file $xml_file";
  $output = shell($cmd);
  //print "<xmp>$cmd\n$output</xmp>";  //debug

  return $output;
}

function process_book($transformer, $arr_params =array(), $lng =UNDEFINED)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $output =process_chunk($transformer, $arr_params, 
                         $book_id, $lng, '--xinclude');
  return $output;  
}

/**
 * Apply a transformation (stylesheet) to the given chunk
 * and then write the result back to the same chunk.
 */
function transform_chunk($transformer, $lng, $arr_params =array(),
                         $chunk =UNDEFINED)
{
  if ($chunk==UNDEFINED)  $chunk = WebApp::getSVar('docbook->chunk');

  $book_id = WebApp::getSVar('docbook->book_id');

  //process chunk with an xsl, and get the new chunk
  $chunk_content = process_chunk($transformer, $arr_params, $chunk, $lng);

  //write back the transformed chunk
  $chunk_file = WS_BOOKS."$book_id/$lng/$chunk.xml";
  write_file($chunk_file, $chunk_content);

  //commit in svn
  $params = "action=\"$action\", ".array2str($arr_params);
  $commit_msg = date('Y-m-d H:i').' >> Modified by '.USER. ' >> '.$params;
  shell("svn commit $chunk_file -m '$commit_msg'");

  //update the public copy
  $tag = book_fixed_to_tag($book_id, $lng);
  if ($tag)
    {
      $msg = T_("The book (v_book_id, v_lng) is fixed to v_tag, \n\
so the changes will not be displayed in the public copy.");
      $msg = str_replace('v_book_id', $book_id, $msg);
      $msg = str_replace('v_lng', $lng, $msg);
      $msg = str_replace('v_tag', $tag, $msg);
      WebApp::message($msg);
    }
  else
    {
      $public_chunk_file = BOOKS."$book_id/$lng/$chunk.xml";
      shell("svn update $public_chunk_file");
    }
}

/**
 * Modifies the chunk for all the languages,
 * by applying the given action (transformation).
 */
function transform_subchunks_all_langs($action, $arr_params =array(),
                                       $chunk =UNDEFINED)
{
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      transform_chunk("edit/$action.xsl", $lng, $arr_params, $chunk);
    }
}

/** Update the xhtml files for all languages. */
function update_xhtml_all_langs()
{
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      update_xhtml_files($lng);
    }
}

/** Update the xhtml files for the given language. */
function update_xhtml_files($lng)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $xhtml_allchunks_sh = CONTENT.'xhtml/xhtml-allchunks.sh';

  //update the xhtml files in the workspace
  $cmd = "$xhtml_allchunks_sh $book_id $lng workspace";
  $output = shell($cmd);
  //$function = 'edit_funcs.php::update_xhtml_files()';  //debug
  //WebApp::debug_msg("\n $cmd \n $output", $function);  //debug

  //update the xhtml files in the public copy
  if (!book_fixed_to_tag($book_id, $lng))
    {
      $cmd = "$xhtml_allchunks_sh $book_id $lng books";
      $output = shell($cmd);
      //$function = 'edit_funcs.php::update_xhtml_files()';  //debug
      //WebApp::debug_msg("\n $cmd \n $output", $function);  //debug
    }
}

function delete_removefiles_all_langs($chunk)
{
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      delete_removefiles($chunk, $lng);
    }
}

function delete_removefiles($chunk, $lng)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $chunk_files = WS_BOOKS."$book_id/$lng/$chunk.xml";
  $chunk_dir = WS_BOOKS."$book_id/$lng/$chunk";
  if (file_exists($chunk_dir))  $chunk_files .= " $chunk_dir";

  $msg = date('Y-m-d H:i') . " >> Deleted by " . USER
    . " >> chunk='$chunk'";

  //remove from svn and commit
  shell("svn remove --force $chunk_files");
  shell("svn update $chunk_files");
  shell("svn commit -m '$msg' $chunk_files");

  //remove the state file
  $state_file = WS_BOOKS."$book_id/$lng/$chunk.state.txt";
  shell("rm -rf $state_file");
  

  //update the public space files
  if (!book_fixed_to_tag($book_id, $lng))
    {
      //update xml files
      $chnk = BOOKS."$book_id/$lng/$chunk";
      shell("svn update $chnk.xml");
      shell("svn update $chnk");
    }
}

/** create the files for a new chunk */
function create_new_chunk($id, $title, $type)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $chunk = WebApp::getSVar('docbook->chunk');

  $new_chunk = "$chunk/$id";

  // chunk.xml and chunk.state.txt
  $chunk_xml = "<?xml version='1.0' encoding='utf-8' standalone='no'?>
<$type id='$id'>
  <title>$title</title>
  <para/>
</$type>";
  $state_txt = "unlocked::::\nadded:".USER.':'.EMAIL.':'.time();
  //commit message
  $msg_params = "parent_chunk='$chunk', new_chunk_id='$id'";
  $commit_msg = date('Y-m-d H:i'). ' >> Added by '.USER.' >> '.$msg_params;

  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];

      //create the directory in case that does not exist
      $chunk_dir  = WS_BOOKS."$book_id/$lng/$chunk";
      if (!file_exists($chunk_dir))
	{
	  shell("mkdir -p $chunk_dir");
	  shell("chmod +x $chunk_dir");
	  shell("svn add $chunk_dir");
	  shell("svn commit $chunk_dir -m '$commit_msg'");
	}
      //create the xml chunk and add it in svn
      $xml_file   = WS_BOOKS."$book_id/$lng/$new_chunk.xml";
      write_file($xml_file, $chunk_xml);
      shell("svn add $xml_file");
      shell("svn commit $xml_file -m '$commit_msg'");
      //create the state file
      $state_file = WS_BOOKS."$book_id/$lng/$new_chunk.state.txt";
      write_file($state_file, $state_txt);

      //create the directories and files in the public copy
      if (!book_fixed_to_tag($book_id, $lng))
        {
          //update the xml from svn
          $public_chunk_dir = BOOKS."$book_id/$lng/$chunk";
          shell("svn update $public_chunk_dir");
        }
    }
}

/** Returns true if the new id is OK, otherwise returns false. */
function change_id_validate($new_id)
{
  $chunk = WebApp::getSVar('docbook->chunk');
  $book_id = WebApp::getSVar('docbook->book_id');

  //get the current node_id 
  $old_id = basename($chunk);

  if ($new_id==$old_id)  return false;

  //the id of the root must not be changed
  if ($old_id==$book_id)
    {
      WebApp::message(T_("The book ID cannot be changed."));
      return false;
    }

  //the id cannot be empty
  if ($new_id=='')
    {
      $msg = T_("The id cannot be empty.");
      WebApp::message($msg);
      return false;
    }

  //check that the new id does not already exist
  /*
  $output = process_index('edit/get_node_id.xsl', array('id'=>$new_id));
  if ($output<>'')
    {
      $msg = T_("The id 'v_new_id' is already used by another section.");
      $msg = str_replace('v_new_id', $new_id, $msg);
      WebApp::message($msg);
      return false;
    }
  */

  //check that this node is not referenced by any other nodes
  /*
  $refs = get_node_references();
  if ($refs != '')
    {
      $msg = T_("This node id cannot be changed because it is referenced by \n  v_refs \nFirst delete the references.");
      $msg = str_replace('v_refs', $refs, $msg);
      WebApp::message($msg);
      return false;
    }
  */

  return true;
}

/** rename the directory of the chunk, for all the languages */
function changeid_movedirs($old_chunk, $new_chunk)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];

      //move the directory in the workspace
      $old_dir = WS_BOOKS."$book_id/$lng/$old_chunk";
      $new_dir = WS_BOOKS."$book_id/$lng/$new_chunk";
      $parent_dir = dirname($new_dir);
      $msg_params = "old_chunk='$old_chunk', new_chunk='$new_chunk'";
      $msg = date('Y-m-d H:i').' >> Changed node id by '.USER.' >> '.$msg_params;
      $status = 'synchronized:'.USER.':'.EMAIL.':'.time();
      shell("svn update $parent_dir");
      shell("svn move --force $old_dir $new_dir");
      shell("svn move --force $old_dir.xml $new_dir.xml");
      shell("svn commit $parent_dir -m '$msg'");
      //move the state file
      shell("mv -f $old_dir.state.txt $new_dir.state.txt");

      //update the files in the public space
      if (!book_fixed_to_tag($book_id, $lng))
        {
	  $public_parent_dir = BOOKS."$book_id/$lng/".dirname($new_chunk);
          shell("svn update $public_parent_dir");
        }
    }
}

/**
 * Find the nodes that reference this one and return a list of them
 * as a string (having a line for each node). The return value is
 * empty string ('') if there are no referencies.
 *
 * This function is used when a node is deleted or when the node id
 * is changed, in order to make sure that there are no dangling
 * referencies in the document. Otherwise the document will become
 * invalid and will fail to convert to other formats. 
 */
/*
function get_node_references()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $lng = WebApp::getSVar('docbook->lng');
  $node_path = WebApp::getSVar('docbook->node_path');

  //get node_id
  $node_id = basename(substr($node_path,0,-1));

  //grep for any sections that reference this one
  $grep = "grep -R '<xref linkend=\"$node_id\"' ".WS_BOOKS."$book_id/";
  $output = shell($grep);

  if (trim($output)=='')  return '';  //there are no referencies

  //extract referencing nodes from the output
  $arr_lines = explode("\n", $output);
  for ($i=0; $i < sizeof($arr_lines); $i++)
    {
      $line = trim($arr_lines[$i]);
      if ($line=='') continue;

      $line = ereg_replace('/content\\.xml: .*', '', $line);
      $line = str_replace(WS_BOOKS, '', $line);
      $arr_refs[] = $line;
    }

  $refs = implode("\n", $arr_refs);
  return $refs;
}
*/
?>