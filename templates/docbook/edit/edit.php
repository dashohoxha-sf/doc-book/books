<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once dirname(__FILE__).'/edit_funcs.php';

/**
 * Edit the content of the current node.
 *
 * @package docbook
 * @subpackage edit
 */
class edit extends WebObject
{
  function on_set_lock($event_args)
  {
    $lock = $event_args['lock'];
    set_node_lock($lock, 'edit');
  }

  function get_subchunk_href($subchunk)
  {
    $subchunk_id = basename($subchunk);
    $chunk_id = basename(dirname($subchunk));
    //$chunk_id = basename(WebApp::getSVar('docbook->chunk'));
    $href = "$chunk_id/$subchunk_id.xml";
    return $href;
  }

  function on_move_up($event_args)
  {
    $subchunk = $event_args['subchunk'];
    $arr_params = array('href' => $this->get_subchunk_href($subchunk));

    lock_book();
    transform_subchunks_all_langs('move_up', $arr_params);
    //update_xhtml_all_langs();
    unlock_book();
  }

  function on_move_down($event_args)
  {
    $subchunk = $event_args['subchunk'];
    $arr_params = array('href' => $this->get_subchunk_href($subchunk));

    lock_book();
    transform_subchunks_all_langs('move_down', $arr_params);
    //update_xhtml_all_langs();
    unlock_book();
  }

  function on_delete($event_args)
  {
    //make sure that the node is not locked by somebody else
    if (locked_by_somebody())  return;

    $chunk = WebApp::getSVar('docbook->chunk');
    $parent_chunk = dirname($chunk);
    $book_id = WebApp::getSVar('docbook->book_id');

    //check that this is not the root chunk
    if ($chunk==$book_id)
      {
        WebApp::message(T_("The root chunk cannot be deleted."));
        return;
      }

    //check that this node is not referenced by any other nodes
    /*
    $refs = get_node_references();
    if ($refs != '')
      {
        //display a message
        $msg = T_("This node cannot be deleted because it is referenced by \n  v_refs \nFirst delete the references.");
        $msg = str_replace('v_refs', $refs, $msg);
        WebApp::message($msg);
  
        return;
      }
    */

    lock_book();

    //remove the link from the parent chunk
    $chunk_id = basename($chunk);
    $parent_chunk_id = basename($parent_chunk);
    $href = "$parent_chunk_id/$chunk_id.xml";
    $arr_params = array('href' => $href);
    transform_subchunks_all_langs('delete', $arr_params, $parent_chunk);

    //remove the files of the current chunk
    delete_removefiles_all_langs($chunk);

    //set the parent chunk as current
    WebApp::setSVar('docbook->chunk', $parent_chunk);

    //update xhtml files
    update_xhtml_all_langs();

    unlock_book();
  }

  function on_add_subchunk($event_args)
  {
    $id = $event_args['id'];
    $title = $event_args['title'];
    $type = $event_args['type'];

    //check that $id does not already exist in index.xml
    /*
    $output = process_index('edit/get_node_id.xsl', array('id'=>$id));
    if ($output==$id)
      {
        $msg = T_("The id 'v_id' is already used by another section.");
        $msg = str_replace('v_id', $id, $msg);
        WebApp::message($msg);
        return;
      }
    */

    lock_book();

    //add a new subchunk to the chunk xml file
    $chunk = WebApp::getSVar('docbook->chunk');
    $chunk_id = basename($chunk);
    $params = array('href' => "$chunk_id/$id.xml");
    transform_subchunks_all_langs('add_subchunk', $params, $chunk);

    //create the files for a new chunk
    create_new_chunk($id, $title, $type);

    //update xhtml files
    update_xhtml_all_langs();

    unlock_book();
  }

  function on_change_id($event_args)
  {
    //make sure that the chunk is not locked by somebody else
    if (locked_by_somebody())  return;

    $new_id = trim($event_args['id']);

    //validate the new id
    if (!change_id_validate($new_id))  return;

    lock_book();

    $book_id = WebApp::getSVar('docbook->book_id');
    $chunk = WebApp::getSVar('docbook->chunk');
    $new_chunk = dirname($chunk).'/'.$new_id;

    //set the new id in the xml chunk file
    $params = array('new_id' => $new_id);
    transform_subchunks_all_langs('change_chunk_id', $params, $chunk);

    //change the href of <xi:include> in the parent chunk
    $chunk_up = dirname($chunk);  //the parent chunk
    $old_id = basename($chunk);
    $parent_id = basename($chunk_up); //id of the parennt chunk
    $old_href = "$parent_id/$old_id.xml";
    $new_href = "$parent_id/$new_id.xml";
    $params = compact('old_href', 'new_href');
    transform_subchunks_all_langs('change_subchunk_href', $params, $chunk_up);

    //update the state variable docbook->chunk with the new chunk path
    $old_chunk = WebApp::getSVar('docbook->chunk');
    $new_chunk = dirname($old_chunk).'/'.$new_id;
    WebApp::setSVar('docbook->chunk', $new_chunk);

    //move folders to the new node path
    changeid_movedirs($old_chunk, $new_chunk);

    //update xhtml files
    update_xhtml_all_langs();

    unlock_book();
  }

  function on_change_title($event_args)
  {
    //make sure that the node is not locked by somebody else
    if (locked_by_somebody())  return;

    lock_book();

    $new_title = $event_args['title'];
    $chunk = WebApp::getSVar('docbook->chunk');

    //set the new title in the xml chunk
    $xsl_file = XSLT."edit/set_chunk_title.xsl";
    $params = "--stringparam title \"$new_title\" ";
    $xml_file = WS_BOOKS."$book_id/$lng/$chunk.xml";
    $xml_content = shell("xsltproc $params $xsl_file $xml_file");
    write_file($xml_file, $xml_content);

    //update the xhtml chunk
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $xhtml_onechunk = CONTENT.'xhtml/xhtml-onechunk.sh';
    shell("$xhtml_onechunk $book_id $lng workspace");

    //add this chunk in the list of modified chunks
    add_to_modified_chunks();

    //set the status of the chunk to modified
    set_chunk_status('modified');

    unlock_book();
  }

  function onParse()
  {
    //add node types
    $chunk_types = process_chunk('edit/get_chunk_type.xsl');
    list($chunk_type,$subchunk_type) = explode(':', chop($chunk_types));
    WebApp::addGlobalVars(compact('chunk_type','subchunk_type'));
  }

  function onRender()
  {
    //add variables {{status}}, {{m_user}}, {{m_email}}, {{m_time}}
    $arr_state = get_chunk_state();
    WebApp::addVar('status', $arr_state['status']);
    $m_user = $arr_state['m_user'];
    $m_email = $arr_state['m_email'];
    WebApp::addVar('m_user', "<a href='mailto:$m_email'>$m_user</a>");
    $m_time = get_date_str($arr_state['m_timestamp']);
    WebApp::addVar('m_time', $m_time);

    //add the variables {{locked_by_somebody}} and {{locked}}
    $locked_by_somebody = locked_by_somebody($arr_state);
    $str_locked_by_somebody = ($locked_by_somebody ? 'true' : 'false');
    $str_locked = (is_locked($arr_state) ? 'true' : 'false');
    WebApp::addVar('locked_by_somebody', $str_locked_by_somebody);
    WebApp::addVar('locked', $str_locked);

    //display a notification message if the chunk is locked
    if ($locked_by_somebody)
      {
        extract($arr_state);
        $msg = T_("This chunk is locked for v_mode\n\
by v_l_user (v_l_email).\n\
Please try again later.");
        $msg = str_replace('v_mode', $mode, $msg);
        $msg = str_replace('v_l_user', $l_user, $msg);
        $msg = str_replace('v_l_email', $l_email, $msg);
        WebApp::message($msg);
        WebApp::addVar('l_mode', $mode);
        WebApp::addVar('l_user', "<a href='mailto:$l_email'>$l_user</a>");
        return;
      }

    //add {{id}}
    $chunk = WebApp::getSVar('docbook->chunk');
    $id = basename($chunk);
    WebApp::addVar("id", $id);

    //add the recordset 'chunktypes'
    $this->add_chunktypes_rs();

    //add the recordset 'subchunks'
    add_subchunks_rs();
  }

  /**
   * Create and add to webPage the recordset 'chunk_types' with the types
   * of the subchunks that can be added to the current chunk.
   */
  function add_chunktypes_rs()
  {
    $rs = new EditableRS('chunk_types');

    $chunk_type = WebApp::getVar('chunk_type');
    switch ($chunk_type)
      {
      case 'book':
	$arr_types = array('chapter', 'appendix', 'preface', 'bibliography');
	break;
      case 'article':
	$arr_types = array('section', 'appendix', 'bibliography');
	break;
      case 'preface':
      case 'chapter':
      case 'section':
      case 'appendix':
	$arr_types = array('section', 'simplesect', 'bibliography');
	break;
      default:
      case 'bookinfo':
      case 'articleinfo':
      case 'bibliography':
      case 'simplesect':
	$arr_types = array();
	break;
      }

    for ($i=0; $i < sizeof($arr_types); $i++)
      {
	$t = $arr_types[$i];
	$rs->addRec(array('id' => $t, 'label' => $t));
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>