#!/bin/bash
### called from approve.php; sets the state to synchronized 
### for the given node and all the subnodes

### get the parameter
node_dir=$1

### get a list of files
file_list=$(find $node_dir -name 'state.txt')

### set the value of state to synchronized
sed -i -e '2 s/^[^:]*/synchronized/' $file_list
