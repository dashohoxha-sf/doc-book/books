<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Displays a section of docbookwiki_guide.
   */
class help extends WebObject
{
  /** this event is called by xref.php */
  function on_select($event_args)
  {
    $book_id = 'docbookwiki_guide';

    //get languages
    $arr_langs = $this->get_langs($book_id);
    $langs = implode(',', $arr_langs);
    $lng = $event_args['lng'];
    if ($lng=='') $lng = $arr_langs[0];

    //get node path from node id
    $node_id = $event_args['topic'];
    $xsl = XSLT.'edit/get_node_path.xsl';
    $index_xml = BOOKS."$book_id/$lng/index.xml";
    $param_id = "--stringparam id '$node_id'";
    $node_path = shell("xsltproc $param_id $xsl $index_xml");
    if ($node_path=='')  $node_path = './';

    //set the state variables of the docbook webbox
    WebApp::setSVar('docbook->book_id', $book_id);
    WebApp::setSVar('docbook->node_path', $node_path);
    WebApp::setSVar('docbook->languages', $langs);
    WebApp::setSVar('docbook->lng', $lng);
    WebApp::setSVar('docbook->mode', 'view');

    //set book_title
    $book_title = $this->get_book_title($book_id, $lng);
    WebApp::setSVar('book_title', $book_title);
  }

  function afterParse()
  {
    //disable webnotes
    WebApp::setSVar('webnotes->page_id', '');

    //don't show the logo 'Powered by phpWebApp'
    WebApp::addGlobalVar('hide_webapp_logo', 'true');
  }

  /**
   * Returns an array of languages (en,fr,it,al) 
   * in which the book is available.
   */
  function get_langs($book_id)
  {
    $book_path = BOOKS.$book_id.'/';
    if (!file_exists($book_path))  return array();

    $langs = shell("ls $book_path");
    $arr_langs = explode("\n", chop($langs));

    return $arr_langs;
  }

  function get_book_title($book_id, $lng)
  {
    $book_list = CONTENT.'books/book_list';
    $title = shell("grep '^$book_id:$lng:' $book_list | cut -d: -f3");
    $title = trim($title);
    return $title;
  }
}
?>