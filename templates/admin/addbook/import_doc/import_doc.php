<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Import a new book or article in the system.
   *
   * @package admin
   * @subpackage addbook
   */
class import_doc extends WebObject
{
  /** refresh the list after uploading a file */
  function on_refresh($event_args)
  {
    sleep(1);
  }

  /** delete an uploaded file */
  function on_delete($event_args)
  {
    $fname = $event_args['fname'];
    $xml_file = CONTENT."initial_xml/uploaded/$fname";
    $media_file = ereg_replace('\.xml$', '.media.tgz', $xml_file);
    shell("rm $xml_file");
    shell("rm $media_file");
  }

  function on_import($event_args)
  {
    //running the script may take a long time
    set_time_limit(0);

    //get the arguments
    $book_id = $event_args['book_id'];
    $lng = $event_args['lng'];
    if ($lng=='')  $lng = 'en';
    $fname = $event_args['fname'];

    //get xml_file and media_files to be imported
    $xml_file = "initial_xml/uploaded/$fname";
    $media_files = ereg_replace('\.xml$', '.media.tgz', $xml_file);
    if (! file_exists(CONTENT.$media_files))  $media_files = '';

    //check for any xml errors
    $fpi = '-//OASIS//DTD DocBook XML V4.4//EN';
    $xmllint = "xmllint --noout --dtdvalidfpi '$fpi' --nonet --nowarning";
    $sed = 'sed -e \$d -e /warning:/d';  
    $xml_errors = shell("$xmllint $xml_file 2>&1 | $sed");
    if (trim($xml_errors)!='')
      {
        //display an error message
        $msg = T_("The XML file 'v_xml_file' cannot be imported because\n\
there are some validation problems (see the output below).\n\
Fix the problems first and try again.");
        $msg = str_replace('v_xml_file', $xml_file, $msg);
        WebApp::message($msg);
        WebApp::debug_msg("<xmp>$xml_errors</xmp>");
        return;
      }

    //there are no errors, import
    $output = shell(CONTENT."import.sh $xml_file $book_id $lng $media_files");
    WebApp::debug_msg("<xmp>$output</xmp>");
  }

  function onRender()
  {
    $this->add_uploaded_files_rs();
  }

  /** create and add the recordset 'uploaded_files' */
  function add_uploaded_files_rs()
  {
    $rs = new EditableRS('uploaded_files');

    $path = CONTENT.'initial_xml/uploaded';
    $output = shell("ls $path/*.xml");
    $arr_files = explode("\n", $output);
    for ($i=0; $i < sizeof($arr_files); $i++)
      {
        $file = $arr_files[$i];
        if ($file=='' or ereg('No such file or directory', $file))  continue;
        $xml_file = str_replace("$path/", '', $file);
        $files = ereg_replace('\.xml$', '', $xml_file);
        $media = ereg_replace('\.xml$', '.media.tgz', $file);
        $files .= (file_exists($media) ? '.{xml,media.tgz}' : '.xml');
        $rs->addRec(compact('xml_file', 'files'));        
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>