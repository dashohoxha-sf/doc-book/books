<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


include_once TPL.'languages/func.languages.php';

  /**
   * Modify the menu of the categories and books.
   * Only the superuser can access this module. 
   *
   * @package admin
   * @subpackage edit_menu
   */
class edit_menu extends WebObject
{
  function init()
  {
    //the menu item that is being edited, initially the root
    $this->addSVar('item_id', 'menu');
    $this->addSVar('item_bookid', '');
    $this->addSVar('item_caption', '');

    //keeps the items that are copied and can be pasted somewhere else
    $this->addSVar('clipboard', '');
  }

  /** select an item for editing */
  function on_select($event_args)
  {
    $item_id = $event_args['item_id'];
    $this->select_item($item_id);
  }

  /**
   * Get the 'bookid' and the 'caption' of the selected item
   * and refresh the state variables.
   */
  function select_item($item_id)
  {
    //get the bookid and the caption of the selected item
    $lng = WebApp::getSVar('language');
    $output = $this->transform('get_bookid.xsl', $lng, array('id'=>$item_id));
    list($bookid, $caption) = split(' ', $output, 2);
    if (!isset($caption)) $caption='';

    //set state variables
    $this->setSVar('item_id', $item_id);
    $this->setSVar('item_bookid', $bookid);
    $this->setSVar('item_caption', $caption);
  }

  /** modify menu for all the languages */
  function transform_menu($transformer, $arr_params =array())
  {
    $arr_langs = array_keys(get_arr_languages());
    for ($i=0; $i < sizeof($arr_langs); $i++)
      {
        $lng = $arr_langs[$i];

        //apply the transformer and get the new menu
        $new_menu = $this->transform($transformer, $lng, $arr_params);

        //save the transformed menu
        $menu_xml = ADMIN."edit_menu/menu/menu_$lng.xml";
        write_file($menu_xml, $new_menu);
      }
  }

  /**
   * Applies the given xsl transformer to menu_lng.xml and returns 
   * the result. $arr_params is an associative array of parameters. 
   */
  function transform($transformer, $lng, $arr_params =array())
  {
    //construct the string $params
    $params = '';
    while (list($p_name, $p_value) = each($arr_params))
      {
        $params .= "--stringparam $p_name \"$p_value\" ";
      }

    //apply the $transformer with $params to menu_lng.xml
    $menu_path = ADMIN.'edit_menu/menu/';
    $menu_xml = $menu_path."menu_$lng.xml";
    $xsl_file = $menu_path."xsl/$transformer";
    $result = shell("xsltproc $params $xsl_file $menu_xml");

    return $result;
  }

  /** add a new subitem to the current item */
  function on_add_subitem($event_args)
  {
    //build the parameters array
    $params = $event_args;
    $params['id'] = $this->getSVar('item_id');
    $params['item_id'] = '';

    //get the array of languages
    $arr_langs = array_keys(get_arr_languages());

    //add a subitem to menu for the first language
    $lng = $arr_langs[0];
    $new_menu = $this->transform('add_subitem.xsl', $lng, $params);
    $menu_xml = ADMIN."edit_menu/menu/menu_$lng.xml";
    write_file($menu_xml, $new_menu);

    //get the id of the subitem that was just added, so that the same id
    //can be used for the other languages as well
    $params1['id'] = $params['id'];
    $item_id = $this->transform('get_lastchild_id.xsl', $lng, $params);
    $params['item_id'] = $item_id;

    //add a subitem to menu for the other languages
    for ($i=1; $i < sizeof($arr_langs); $i++)
      {
        $lng = $arr_langs[$i];
        $new_menu = $this->transform('add_subitem.xsl', $lng, $params);
        $menu_xml = ADMIN."edit_menu/menu/menu_$lng.xml";
        write_file($menu_xml, $new_menu);
      }
  }

  /** delete the current item and set the parent item as the current one */
  function on_delete($event_args)
  {
    $params['id'] = $this->getSVar('item_id');
    $parent_id = $this->transform('get_parent_id.xsl', $params);
    $this->transform_menu('delete.xsl', $params);
    $this->select_item($parent_id);
  }

  /** save modifications in id and caption of the current item */
  function on_save($event_args)
  {
    $new_bookid = $event_args['bookid'];
    $new_caption = $event_args['caption'];
    $item_id = $this->getSVar('item_id');

    //get the bookid and the caption of the current item
    $output = $this->transform('get_bookid.xsl', $lng, array('id'=>$item_id));
    list($bookid, $caption) = split(' ', $output, 2);

    //update bookid
    if ($new_bookid != $bookid)
      {
        $params = array('id' => $item_id, 'bookid' => $new_bookid);
        $this->transform_menu('update_bookid.xsl', $params);
        $this->setSVar('item_bookid', $new_bookid);
      }

    //update caption only for the current language
    if ($new_caption != $caption)
      {
        $lng = WebApp::getSVar('language');
        $params = array('id' => $item_id, 'caption' => $new_caption);
        $new_menu = $this->transform('update_caption.xsl', $lng, $params);
        $menu_xml = ADMIN."edit_menu/menu/menu_$lng.xml";
        write_file($menu_xml, $new_menu);
        $this->setSVar('item_caption', $new_caption);
      }
  }

  /** move up the given item */
  function on_move_up($event_args)
  {
    $params['id'] = $event_args['item_id'];
    $this->transform_menu('move_up.xsl', $params);
  }

  /** move down the given item */
  function on_move_down($event_args)
  {
    $params['id'] = $event_args['item_id'];
    $this->transform_menu('move_down.xsl', $params);
  }

  /** add any items in clipboard as subitems of the current item */
  function on_paste($event_args)
  {
    $clipboard = $this->getSVar('clipboard');
    $clipboard_items = explode("\n", $clipboard);

    //remove any doublicated items in clipboard
    $arr_items = array();
    for ($i=0; $i < sizeof($clipboard_items); $i++)
      {
        $item = trim($clipboard_items[$i]);
        if ($item=='')  continue;
        if (in_array($item, $arr_items))  continue;
        $arr_items[] = $item;
      }

    //add the items of the clipboard as subitems
    $item_id = $this->getSVar('item_id');
    for ($i=0; $i < sizeof($arr_items); $i++)
      {
        $item = $arr_items[$i];
        list($copy_id, $bookid, $caption) = split(':', $item, 3);

        //add the subitem
        $params['id'] = $item_id;
        $params['copy_id'] = $copy_id;
        $this->transform_menu('copy_item.xsl', $params);
      }
  }

  /** apply the modifications to the main menu */
  function on_apply($event_args)
  {
    $arr_langs = array_keys(get_arr_languages());
    for ($i=0; $i < sizeof($arr_langs); $i++)
      {
        $lng = $arr_langs[$i];

        //update menu_lng.xml
        $menu_xml = MENU."menu_$lng.xml";
        $edit_menu_xml = ADMIN."edit_menu/menu/menu_$lng.xml";
        shell("cp $edit_menu_xml $menu_xml");

        //update menu_items_lng.js
        $menu_items = $this->transform('menu_items.xsl', $lng);
        write_file(MENU."menu_items_$lng.js", $menu_items);
      }

    //select the root item
    $this->select_item('menu');
  }

  /** discard any modifications and get a copy of the main menu */
  function on_cancel($event_args)
  {
    $arr_langs = array_keys(get_arr_languages());
    for ($i=0; $i < sizeof($arr_langs); $i++)
      {
        $lng = $arr_langs[$i];

        $menu_xml = MENU."menu_$lng.xml";
        $edit_menu_xml = ADMIN."edit_menu/menu/menu_$lng.xml";
        shell("cp $menu_xml $edit_menu_xml");
      }

    //select the root item
    $this->select_item('menu');
  }

  function onRender()
  {
    $this->add_subitems_rs();
    $this->add_books_rs();
  }

  /** add a recordset of the subitems of the selected item */
  function add_subitems_rs()
  {
    //get the subitems
    $lng = WebApp::getSVar('language');
    $item_id = $this->getSVar('item_id');
    $items = $this->transform('subitems.xsl', $lng, array('id'=>$item_id));
    $arr_lines = explode("\n", $items);

    //create a recordset with id-s and captions of the subitems
    $rs = new EditableRS('subitems');
    for ($i=0; $i < sizeof($arr_lines); $i++)
      {
        list($id, $bookid, $caption) = split(' ', $arr_lines[$i], 3);
        if ($id=='')  continue;
        $rs->addRec(compact('id', 'bookid', 'caption'));
      }
    global $webPage;
    $webPage->addRecordset($rs);
  }

  /**
   * Add a recordset of all the books,
   * which will be displayed in a listbox.
   */
  function add_books_rs()
  {
    //books
    $rs = new EditableRS('booklist');
    $rs->addRec(array('id'=>'', 'label'=>'----- '.T_("Select").' -----'));
    $output = shell('ls '.BOOKS);
    $arr_lines = explode("\n", $output);
    for ($i=0; $i < sizeof($arr_lines); $i++)
      {
        $book_id = trim($arr_lines[$i]);
        if ($book_id=='')  continue;
        $rs->addRec(array('id' => $book_id, 'label' => $book_id));
      }
    global $webPage;
    $webPage->addRecordset($rs);      
  }
}
?>