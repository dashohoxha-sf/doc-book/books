#!/bin/bash
### enable/disable automatic email notifications on commit

### go to this directory
cd $(dirname $0)

function usage
{
  echo "Usage: $0 [ on | off | ls ]"
}

function enable
{
  ### setup the post-commit script
  if [ -d 'repository' ]
  then
    .  ../../books.conf  #get ADMIN_EMAIL
    cp -f post-commit.tmpl post-commit

    cat <<EOF >> post-commit

$(pwd)/commit-email.pl "\$REPOS" "\$REV" \\
    --from $ADMIN_EMAIL -s '[docbookwiki]' $ADMIN_EMAIL

EOF

    chmod +x post-commit
    ln -s ../../post-commit repository/hooks/post-commit
  fi
}

function disable
{
  rm -f repository/hooks/post-commit
  rm -f post-commit
}

function show
{
  if [ -f post-commit ]
  then echo "on"
  else echo "off"
  fi
}

### get the argument
if [ "$1" = "" ]
then
  usage
  exit 1
fi
arg=$1

case $arg in
  on  )  enable ;;
  off )  disable ;;
  ls  )  show ;;
  *   )  usage ;;
esac
