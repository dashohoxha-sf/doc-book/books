#!/bin/bash
### add in svn a new book

### go to this dir
cd $(dirname $0)

### get the parameters
if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  echo "where lng can be en, de, fr, it, sq_AL, etc. (default is en)"
  exit 1;
fi
book_id=$1
lng=${2:-en}

### make sure that SVN is initialized
if [ ! -f svn_cfg.txt ]
then
  echo "SVN is not initialized, run '$(dirname $0)/init.sh' first"
  exit 2
fi

### check that the book is not already there
if grep "$book_id:$lng" book_list
then
  echo "$book_id:$lng is already in the book_list"
  exit 3
fi

### read the name of the svn sandbox from svn_cfg.txt
. svn_cfg.txt

###----------- add the xml file -----------------

### xml_file
xml_file=$svn_dir/${book_id}_${lng}.xml

### implode and copy the xml file
../implode/implode.sh $book_id $lng
cp ../implode/tmp/$book_id.xml $xml_file

### add in svn and commit xml file
svn add $xml_file
svn commit $xml_file -m \""$(hostname -i):$(pwd)"\"

###------------ add the media files -------------

### media dir
media_dir="$svn_dir/media/$book_id/$lng"    # svn_dir/media/book_id/lng

### create and add in svn the parent dir (if it does not exist)
media_dir_1=$(dirname $media_dir)  # parent dir
if [ ! -d $media_dir_1 ]
then
  mkdir -p $media_dir_1
  svn add $media_dir_1
  svn commit $media_dir_1 -m ''  
fi

### get media files
../implode/get_media.sh $book_id $lng

### copy media files to media_dir
mkdir -p $media_dir
if [ -f ../implode/tmp/$book_id.media.tgz ]
then
  tar xfz ../implode/tmp/$book_id.media.tgz -C $media_dir
  rm ../implode/tmp/$book_id.media.tgz
fi
svn add $media_dir
svn commit $media_dir -m \""$(hostname -i):$(pwd)"\"

###-----------------------------------------------

### append book to the book_list
echo "$book_id:$lng" >> book_list
