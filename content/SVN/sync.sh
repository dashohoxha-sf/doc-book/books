#!/bin/bash
### syncronize a book with svn

### go to this dir
cd $(dirname $0)

### get arguments
if [ "$1" = "" ]
then
  echo "Usage: $0 book-id [lng]"
  echo "where lng is: en, de, fr, it, sq_AL, etc. (default is en)"
  exit 1;
fi
book_id=$1
lng=${2:-en}

### read the name of the svn sandbox from svn_cfg.txt
. svn_cfg.txt

### variables
xml_file=$svn_dir/${book_id}_${lng}.xml
media_dir="$svn_dir/media/$book_id/$lng"    # svn_dir/media/book_id/lng

### implode the book with any latest changes
../implode/implode.sh $book_id $lng
mv ../implode/tmp/$book_id.xml $xml_file

### update from the svn repository
echo "Updating  '$xml_file'"
svn update $xml_file

### commit xml_file to the svn repository
echo "Commiting '$xml_file'"
svn commit $xml_file -m "$(hostname -i):$(pwd)"

### explode and refresh the cache
./update.sh $book_id $lng

#### update media from the svn repository
#echo "Updating  '$media_dir'"
#if [ -d $media_dir ]; then svn update $media_dir; fi
#
#### get media files and commit to the svn repository
#../implode/get_media.sh $book_id $lng
#if [ -f ../implode/tmp/$book_id.media.tgz ]
#then
#  rm -rf tmp_media/
#  mkdir tmp_media/
#  tar xfz ../implode/tmp/$book_id.media.tgz -C tmp_media/
#  rm ../implode/tmp/$book_id.media.tgz
#  echo "Commiting '$media_dir'"
#  ./svn_load_dirs.pl -wc $media_dir \
#                     $repository/trunk/ media/$book_id/$lng/  tmp_media/
#  rm -rf tmp_media/
#fi

