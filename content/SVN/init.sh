#!/bin/bash
### initialise the svn repository

### go to this dir
cd $(dirname $0)

### get the arguments
if [ "$1" = "help" ]
then
  echo "Usage: $0 [svn_dir] [repository]
where 'svn_dir'     is the name of the SVN directory that will keep the docs
and   'repository'  is the SVN repository, e.g. http://server/path" 
  exit 1
fi
svn_dir=${1:-my_docs}
repository=$2

### check that it is not initialized yet
if [ -f svn_cfg.txt ]
then
  echo "$0: It seems to be already initialized, use '$(dirname $0)/clean.sh' first"
  exit 2
fi

### if no repository argument is given, create a local repository
if [ "$repository" = "" ]
then
  # create it if it does not exist
  if [ ! -d 'repository' ]
  then
    svnadmin create $(pwd)/repository  #create
  fi
  repository=file://$(pwd)/repository 
fi

### save $svn_dir and $repository in a file for later use
cat <<EOF > svn_cfg.txt
repository='$repository'
svn_dir='$svn_dir'
EOF

### create the directories 'trunk/media/' in the svn repository
svn mkdir $repository/trunk/ -m ''
svn mkdir $repository/trunk/media/ -m ''

### checkout trunk to svn_dir
rm -rf $svn_dir
echo "Checking out '$repository/trunk/' in '$svn_dir'"
svn checkout $repository/trunk/ $svn_dir

### add each book in the book_list
if [ ! -f book_list ]; then touch book_list; fi
for book in $(< book_list)
do
  book_id=${book%:*}
  lng=${book#*:}
  ./add_book.sh $book_id $lng
done

### checkout trunk to ../downloads/xml_source/
xml_source=../downloads/xml_source/
rm -rf $xml_source
echo "Checking out '$repository/trunk/' in '$xml_source'"
svn checkout $repository/trunk/ $xml_source
