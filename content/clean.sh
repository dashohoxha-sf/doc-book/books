#!/bin/bash
### remove a book or all the books

# go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 [book_id | all] [lng]"
  exit 1;
fi

book_id=$1
lng=$2

if [ "$1" = "all" ]
then
  book_id=''
  bookid_lng=''
else
  bookid_lng=$book_id/$lng
fi

dir_list="svn
          books/xml
          books/xhtml
          workspace/xml
          workspace/xhtml
          downloads/tar_gz
          downloads/formats"
for dir in $dir_list
do
  rm -rf $dir/$bookid_lng
done

### remove the book from books/book_list
if [ "$1" = "all" ]
then sedexp="/./d"
else sedexp="/^$book_id:$lng/d"
fi
if [ -f books/book_list ]
then
  sed -i $sedexp books/book_list
fi

### if clean all, then clean some additional things
if [ "$1" = "all" ]
then
  # clean books/, workspace/
  rm -rf books/ workspace/

  # clean search indexes
  rm -f ../search/*.index*
fi

