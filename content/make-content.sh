#!/bin/bash
### creates the content of the folders books/, books_html/,
### workspace/books/, etc. by exploding the initial xml files

### go to this dir
cd $(dirname $0)

### create some directories
mkdir -p books/svn workspace initial_xml/uploaded

### make sure that SVN is initialized
if [ ! -f SVN/svn_cfg.txt ]; then SVN/init.sh; fi

#book_list="docbookwiki_guide
#           slackware_router
#           gateway_server
#           tools_and_apps
#           install_exim
#           linux_server_config
#           lcg_install
#           inima_soft_server
#           inima_gateway_server"
book_list="docbookwiki_guide slackware_router"

### import books in the system
for book_id in $book_list
do
  ./import.sh initial_xml/${book_id}_en.xml   \
              $book_id en                     \
              initial_xml/$book_id.media.tgz
done

### import a test book
./import.sh initial_xml/docbookwiki_guide_en.xml     \
            test en                                  \
            initial_xml/docbookwiki_guide.media.tgz

### linux_server_admin
cd initial_xml
tar xfz linux_server_admin.tgz
xsltproc -o linux_server_admin_en.xml transform.xsl \
         linux_server_admin/linux_server_admin.xml
cd ..
file_xml=initial_xml/linux_server_admin_en.xml
./import.sh $file_xml linux_server_admin
./import.sh $file_xml linux_server_admin sq_AL
#./import.sh $file_xml linux_server_admin de
#./import.sh $file_xml linux_server_admin nl
#./import.sh $file_xml linux_server_admin it
rm -rf initial_xml/linux_server_admin/

### kushtetuta
#kushtetuta_al=initial_xml/kushtetuta_al.xml
#./import.sh $kushtetuta_al kushtetuta sq_AL
#./import.sh $kushtetuta_al kushtetuta en
#./import.sh $kushtetuta_al kushtetuta de
#./import.sh $kushtetuta_al kushtetuta nl

### kushtetuta_it
#./import.sh initial_xml/kushtetuta_it.xml kushtetuta_it it
