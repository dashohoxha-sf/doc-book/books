#!/bin/bash
### Put the given tgz media files to the xml directory of the book.

### go to the content dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 media_files.tgz [book-id] [lng]"
  echo "Put the given media files to the xml directory of the book.

'media_files.tgz' contains the media files in directories according
to the sections and subsections. If the parameter book_id is missing,
then the basename of media_files.tgz is used, with everything after
the first '.' removed. If the parameter lng is missing, en is taken by default.
"
  exit 1;
fi

media_files=$1
book_id=$2
lng=${3:-en}

if [ "$book_id" = "" ]
then
  book_id=$(basename $media_files)
  book_id=${book_id%%.*}
fi

### copy media files to the workspace directory of the book
book_dir=workspace/xml/$book_id/$lng
if [ ! -d $book_dir ]
then
  echo "$0: Could not find '$book_dir'"
  echo "$0: Aborting..."
  exit 1
fi
if [ -f $media_files ]
then
  tar xfz $media_files -C $book_dir
fi

### get a list of all the media files
media_files=$(
find $book_dir  \( -name .svn -prune \)  \
             -o \( -type f -a ! -name '*.xml' -a ! -name '*.txt' -print \)
)

### add and commit to svn all media files
if [ "$media_files" != "" ]
then
  svn add $media_files
  svn commit -m 'adding media files' $media_files
fi

### update the public dir
svn update books/xml/$book_id/$lng

### commit modifications in SVN
SVN/commit.sh $book_id $lng