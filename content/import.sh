#!/bin/bash
### Import a DocBook document in the DocBookWiki system.

### go to this dir
cd $(dirname $0)

### check parameters
if [ "$1" = "" ]
then
  echo "
Usage: $0 file.xml [book-id] [lng] [media-files.tgz]

Param file.xml is the DocBook document (book or article) to be imported.

Param book-id is the id of the book; if missing, it is taken from the
      attribute 'id' of the root element (book or article) of file.xml.

Param lng can be: en, en_US, en_US.UTF-8, sq_AL, etc.; if missing it is
      taken from the attribute 'lang' of file.xml (default is en).

Param 'media-files.tgz' contains the media files of the book, organized in
      directories and subdirectories according to the id-s of sections and 
      subsections to which they belong. If missing, then 'file.media.tgz'
      will be tried (where '.xml' is replaced by '.media.tgz' in 'file.xml').

Note: the path of the files 'file.xml' and 'media-files.tgz' should be 
      either absolute, or relative to the 'content/' directory.

"
  exit 1
fi

### get parameters
xml_file=$1
book_id=$2
lng=${3:-en}
media_files=$4

### check that the xml_file does exist
if [ ! -f $xml_file ]
then
  echo "$0: Error: file not found: $xml_file
    The path of the files 'file.xml' and 'media-files.tgz' should be 
    either absolute, or relative to the 'content/' directory."
  exit 2
fi

### if book_id and lng are not given, get them from the attributes
### id and lang of the root element (book or article) of the xml file
if [ "$book_id" = "" ]
then
  xslt=../xslt
  book_id=$(xsltproc $xslt/common/get-id.xsl $xml_file)
  lng=$(xsltproc $xslt/common/get-lang.xsl $xml_file)
  lng=${lng:-en}  #default is english
fi

### if book_id is still undefined, stop processing
if [ "$book_id" = "" ]
then
  echo "$0: Error: book_id is undefined."
  exit 3;
fi

### check for media_files
if [ "$media_files" = "" ]
then
  media_files=${xml_file%.xml}.media.tgz
fi

### clean first, in case there is an existing copy
./clean.sh $book_id $lng 

### set some variables
repository=svn/$book_id/$lng
url=file://$(pwd)/$repository

### create a svn repository for this book and language
echo "Creating svn repository $repository"
mkdir -p $repository
svnadmin create $repository

### convert the xml file to a chunked format
chunk/chunk.sh $xml_file $book_id $lng

### import into repository
echo "Importing into:"
echo $url/trunk
svn import chunk/tmp/$book_id/$lng $url/trunk -q -m ""
rm -rf chunk/tmp/$book_id/$lng

### make a directory for tags in the repository 
svn mkdir -m 'directory for tags' $url/tags

### checkout into books/xml/
echo "Checking out to books/xml/$book_id/$lng/"
svn checkout -q $url/trunk books/xml/$book_id/$lng/

### checkout into workspace/xml/
if [ ! -d workspace/xml/$book_id/ ]
then
  mkdir -p workspace/xml/$book_id/
fi
echo "Checking out to workspace/xml/$book_id/$lng/"
svn checkout -q $url/trunk workspace/xml/$book_id/$lng/

### create the xhtml files
echo "Creating xhtml files in books/xhtml/$book_id/$lng/"
xhtml/xhtml-allchunks.sh $book_id $lng 'books'
echo "Creating xhtml files in workspace/xhtml/$book_id/$lng/"
xhtml/xhtml-allchunks.sh $book_id $lng 'workspace'

### get ADMIN_EMAIL from books.conf
.  ../books.conf
user=$(whoami)
email=$ADMIN_EMAIL

### create a state file *.state.txt for each section in the workspace
echo "Creating files *.state.txt in workspace/xml/$book_id/$lng/"
timestamp=$(date +%s)
chunk_list=$(find workspace/xml/$book_id/$lng/ -name '*.xml')
for chunk in $chunk_list
do
  state_file=${chunk/%.xml/.state.txt}
  echo "unlocked::::" > $state_file
  echo "imported:$user:$email:$timestamp" >> $state_file
done

### append book to the book_list
xslt=../xslt
book_title=$(xsltproc $xslt/common/get-title.xsl $xml_file)
echo "$book_id:$lng:$book_title" >> books/book_list

###-------------- import media files -------------------------

### copy media files to the workspace directory of the book
if [ -f $media_files ]
then
  tar xfz $media_files -C workspace/xml/$book_id/$lng
fi

### get a list of all the media files
media_file_list=$(
find workspace/xml/$book_id/$lng  \( -name '.svn' -prune \)  \
     -o \( -type f -a ! -name '*.xml' -a ! -name '*.txt' -print \)
)

### add and commit to svn all media files
if [ "$media_file_list" != "" ]
then
  svn add $media_file_list
  svn commit -m 'adding media files' $media_file_list
fi

### update the public dir
svn update books/xml/$book_id/$lng

###-------------------------------------------------------------

### done
echo "----------"

