#!/bin/bash
### Creates the xhtml file for the given xml chunk (without subchunks).

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book-id chunk [lng] [books-path]"
  echo "'books-path' is either 'books' or 'workspace'"
  exit 1;
fi

book_id=$1
chunk=$2
lng=${3:-en}
books=${4:-"books"}

chunk_id=$(basename $chunk)
chunk_dir=$(dirname $chunk)
xmldoc=../$books/xml/$book_id/$lng/$chunk.xml
xhtml_path=../$books/xhtml/$book_id/$lng/$chunk_dir
xslt=../../xslt

rm -rf $xhtml_path/
mkdir -p $xhtml_path/

### create the xhtml file for the given chunk
xsltproc --stringparam base.dir $xhtml_path/ \
         --stringparam root.filename $chunk_id \
         --stringparam chunk $chunk \
         $xslt/xhtml/xhtml-onechunk.xsl $xmldoc


