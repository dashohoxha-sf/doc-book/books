#!/bin/bash
### checkout the xml sources of the books that will be used
### to generate the downloadables

### go to this dir
cd $(dirname $0)

### get url
url=$(../SVN/get_url.sh)
if [ $url = "" ]
then
  echo "SVN repository uninitialized"
  exit 1
fi

# checkout to directory xml_source
rm -rf xml_source/
svn checkout $url xml_source
