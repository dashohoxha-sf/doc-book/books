#!/bin/bash
### Convert to a single html page, with css that are 
### very similar to the css of the application.
### Has parameters: book_id, lng.
### The input file is 'formats/$book_id/$lng/xml/$book_id.$lng.xml'.
### Media files of the book should be in: 'formats/$book_id/$lng/media/'.
### The output is placed in directory: 'formats/$book_id/$lng/html1/'.

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book_id [html1] [lng]"
  exit 1
}

if [ "$1" = "" ]; then usage; fi
echo "--> $0 $1 $2 $3"

### get parameters
book_id=$1
format=$2
lng=${3:-en}

### set variables
book=$book_id.$lng
book_xml=formats/$book_id/$lng/xml/$book.xml
media=formats/$book_id/$lng/media
output=formats/$book_id/$lng/$format
tmp=formats/tmp
xml_file=$tmp/$book.xml

### make directories
mkdir -p $output
mkdir -p $tmp

### copy the xml file to the temporary directory
cp $book_xml $xml_file
ln -s ../$book_id/$lng/media $tmp/

### generate the requested format
case "$format" in
  html1 )
    outdir=$output/$book.xslt/
    mkdir $outdir
    xsltproc -o $outdir/$book.xslt.html xml2html/xml2html.xsl $xml_file

    ### copy css files there too
    cp -rf xml2html/css $outdir

    ### create a link to the media files of the book
    ln -s ../../media $outdir
    ;;

  * ) usage ;;
esac

### clean up
rm -rf $tmp/