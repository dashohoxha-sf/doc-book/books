#!/bin/bash

### go to this dir
cd $(dirname $0)

### get the translatable strings from the templates
find ../templates/ -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --output=books.po

### get the translatable strings from the php files in the app dir
xgettext -C --keyword=T_ --join-existing --output=books.po ../*.php

### get the translatable strings from webobjects
find ../webobjects/ -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --join-existing --output=books.po
