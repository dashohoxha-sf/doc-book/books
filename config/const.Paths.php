<?php
  //constants of the paths in the application
define('WEBAPP_PATH', 'web_app/');
define('GRAPHICS',    'graphics/');
define('CSS',         'css/');
define('TPL',         'templates/');
define('MENU',        TPL.'main/menu/');
define('DOCBOOK',     TPL.'docbook/');
define('ADMIN',       TPL.'admin/');
define('SCRIPTS',     TPL.'scripts/');

define('XSLT',        'xslt/');
define('WOBJ',        'webobjects/');

define('CONTENT',     'content/');
define('BOOKS',       CONTENT.'books/xml/');
define('SVN',         CONTENT.'books/svn/');
define('XHTML',       CONTENT.'books/xhtml/');

define('WORKSPACE',   'content/workspace/');
define('WS_BOOKS',    WORKSPACE.'xml/');
define('WS_XHTML',    WORKSPACE.'xhtml/');
?>
