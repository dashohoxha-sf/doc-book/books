<?xml version='1.0'?><!-- -*-SGML-*- -->

<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Convert index.xml to docbook format, so that it can be validated.
This is done in order to make sure that the transformations that are
applied to index.xml (move_up, move_down, add_subnode, etc.) are valid.
It is called like this:
  xsltproc index2docbook.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:output method="xml" version="1.0" encoding="utf-8" 
            omit-xml-declaration="no" standalone="no" indent="yes" />


<!-- ignore path attributes -->
<xsl:template match="@path" />

<!-- add a <title/> to bookinfo or articleinfo -->
<xsl:template match="bookinfo | articleinfo">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <title/>
  </xsl:copy>
</xsl:template>

<!-- add a <title/> and a <para/> -->
<xsl:template match="preface | appendix | chapter | section | simplesect">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <title/><para/>
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>

<!-- add a <biblioentry><title/></biblioentry> -->
<xsl:template match="bibliography">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <biblioentry><title/></biblioentry>
  </xsl:copy>
</xsl:template>
 
<!-- copy everything else -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:copy>
</xsl:template>

</xsl:transform>
